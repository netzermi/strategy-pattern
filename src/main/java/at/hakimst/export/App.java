package at.hakimst.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        try {
            Article a1 = new Article(1,"PS4", 300d);
            Article a2 = new Article(2, "PS5", 600d);
            List<Article> articleList = new ArrayList<>();
            articleList.add(a1);
            articleList.add(a2);
            Export export = new Export(new XMLExport());
            export.doExport("export.xml");

        } catch (Exception e){
            e.printStackTrace();
        }

    }




}
