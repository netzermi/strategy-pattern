package at.hakimst.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Export {
    private ExportStrategy exportStrategy;
    private List<Article> articleList;

    public Export(ExportStrategy exportStrategy){
        this.exportStrategy = exportStrategy;
        articleList = new ArrayList<>();
    }

    public Export(ExportStrategy exportStrategy, List<Article> articleList){
        this.exportStrategy = exportStrategy;
        this.articleList = articleList;
    }

    public void doExport(String filepath) throws Exception{
        File file = new File("output.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(exportStrategy.export(articleList));
        bufferedWriter.g();
    }
}
