package at.hakimst.export;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class GregExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Geben Sie das Datum in folgendem Format ein Tag:Monat:Jahr");
        String eingabe = scanner.next();
        String[] teile = eingabe.split(":");
        int jahr = Integer.valueOf(teile[2]);
        int monat = Integer.valueOf(teile[1])-1;
        int tag = Integer.valueOf(teile[0]);
        GregorianCalendar date = new GregorianCalendar(jahr,monat,tag);
        System.out.println(date.getTime());


    }
}
