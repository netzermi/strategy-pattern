package at.hakimst.export;

import java.util.List;

public interface ExportStrategy {

    public String export(List<Article> articleList);
}
