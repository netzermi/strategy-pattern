package at.hakimst.export;

import java.util.List;

public class XMLExport implements ExportStrategy{
    @Override
    public String export(List<Article> articleList) {
        String xml = "<articles>\n";
        for(Article a : articleList){
            xml += "\t<article>\n";
            xml += "\t\t<id>" + a.getId() + "</id>\n";
            xml += "\t\t<name>" + a.getName() + "</name>\n";
            xml += "\t\t<price>" + a.getPrice() + "</price>\n";
            xml += "\t</article>\n";
        }
        xml += "</articles>";
        return xml;
    }
}
