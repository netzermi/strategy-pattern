package at.hakimst.export;

import java.lang.reflect.Field;
import java.util.List;

public class SQLExport implements ExportStrategy{
//    @Override
//    public String export(List<Article> articleList) {
//        String sql = "";
//        for(Article a : articleList){
//            sql += "INSERT INTO articles VALUES (" + a.getId() + ", " +
//            a.getName() + "," +  a.getPrice() + ");\n";
//        }
//        return sql;
//    }

    public String export(List<Article> articleList) {
        String sql ="";
        try {
            Field[] fields = Article.class.getDeclaredFields();
            for (Article a : articleList) {
                sql += "INSERT INTO articles VALUES (";
                for (Field f : fields) {
                    f.setAccessible(true);
                    sql += String.valueOf(f.get(a))+ ",";
                }
                sql += ");\n";
            }

        } catch(Exception e){
            e.printStackTrace();
        }
        return sql;
    }
}
